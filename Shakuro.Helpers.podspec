#
#
#

Pod::Spec.new do |s|
    s.name             = 'Shakuro.Helpers'
    s.version          = '0.50'
    s.summary          = 'Shakuro helpers'
    s.homepage         = 'https://gitlab.com/shakuro-public/helpers'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.authors          = {'Sanakabarabaka' => 'slaschuk@shakuro.com',
                            'wwwpix' => 'spopov@shakuro.com',
			    'apopov1988' => 'apopov@shakuro.com',
			    'vonipchenko' => 'vonipchenko@shakuro.com'}
    s.source           = { :git => 'https://gitlab.com/shakuro-public/helpers.git', :tag => s.version }
    s.swift_version    = '5.0'

    s.ios.deployment_target = '10.0'

    # --- subspecs ---

    s.subspec 'Array+Chunks' do |sp|

        sp.source_files = 'Array+Chunks/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'CALayer+Transition' do |sp|

        sp.source_files = 'CALayer+Transition/Source/**/*'
        sp.frameworks = 'UIKit'
        sp.dependency 'CommonCryptoModule', '1.0.2'

    end

    s.subspec 'Data+Hash' do |sp|

        sp.source_files = 'Data+Hash/Source/**/*'
        sp.frameworks = 'UIKit'
        sp.dependency 'CommonCryptoModule', '1.0.2'

    end

    s.subspec 'Lock+Execute' do |sp|

        sp.source_files = 'Lock+Execute/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'ShortNumberFormatter' do |sp|

        sp.source_files = 'ShortNumberFormatter/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'String+Hash' do |sp|

        sp.source_files = 'String+Hash/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'UIApplication+BundleIdentifier' do |sp|

        sp.source_files = 'UIApplication+BundleIdentifier/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'UICollectionView+Reusable' do |sp|

        sp.source_files = 'UICollectionView+Reusable/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'UIColor+Helpers' do |sp|

        sp.source_files = 'UIColor+Helpers/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'UIDevice+Uptime' do |sp|

        sp.source_files = 'UIDevice+Uptime/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'UIStoryboard+Instantiate' do |sp|

        sp.source_files = 'UIStoryboard+Instantiate/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'UITableView+Reusable' do |sp|

        sp.source_files = 'UITableView+Reusable/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'UIViewController+Child' do |sp|

        sp.source_files = 'UIViewController+Child/Source/**/*'
        sp.frameworks = 'UIKit'

    end

end