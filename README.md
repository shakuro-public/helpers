# Helpers

Shakuro Helpers - small classes for working with our libraries

## Requirements

- iOS 10.0+
- Xcode 9.2+
- Swift 4.0+

## Installation

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate Helpers into your Xcode project, specify it in your `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '10.0'
use_frameworks!

target '<Your Target Name>' do
    pod 'Shakuro.Helpers', :git => 'https://gitlab.com/shakuro-public/helpers', :tag => '1.0'
end
```

Then, run the following command:

```bash
$ pod install
```

You can use/integrate only the necessary components. To do this, you need to specify the subpod:

```ruby
target '<Your Target Name>' do
    pod 'Shakuro.Helpers/<Component Name>', :git => 'https://gitlab.com/shakuro-public/helpers', :tag => '1.0'
#example:
    pod 'Shakuro.Helpers/Array+Chunks', :git => 'https://gitlab.com/shakuro-public/helpers', :tag => '1.0'
end
```

### Manually

If you prefer not to use CocoaPods, you can integrate any/all components from the Shakuro Helpers simply by copying them to your project.

## License

Shakuro Helpers is released under the MIT license. [See LICENSE](https://github.com/shakurocom/iOS_Toolbox/blob/master/LICENSE) for details.
